const path = require('path');
const webpack = require('webpack');

const dev = require('./back/config').app.dev;

let entries = ['./react/index.js'];
let plugins = [];

const output = {
  path: path.resolve('www', 'js'),
  filename: 'bundle.js',
  publicPath: '/js/',
};

const loaders = [{
  test: /\.js$/,
  loaders: ['babel'],
  include: path.resolve('react')
}];

if (dev) {
  entries = [...entries, 'webpack-hot-middleware/client'];
  plugins = [
    ...plugins,
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
  ];
}

module.exports = {
  devtool: dev ? 'source-map' : false,
  entry: entries,
  output: output,
  plugins: plugins,
  module: {
    loaders: loaders,
  },
};