const assert = require('assert');

const Player = require('../../back/api/controllers/player.controller');
const err = require('../../back/utils/err').errors;

describe('Player', () => {

  describe('#constructor', () => {

    it('should create a new Player object with nick "toto"', () => {
      const player = new Player('toto');
      const expected = 'toto';

      assert.equal(player.nick, expected);
    });

    it('should fail to create a new Player with no nick', () => {
      const expected = err.PLAYER_NICK_NOT_SET;

      assert.throws(() => new Player(), err => {
        return err.code == expected;
      });
    });

  });

});