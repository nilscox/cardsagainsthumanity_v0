const Sequelize = require('sequelize');
const winston = require('winston');
const db = require('../config').db;

module.exports = new Sequelize(
  db.database, db.username, db.password, {
    host: db.host,
    port: db.port,
    dialect: db.dialect,
    logging: winston.verbose,
  }
);
