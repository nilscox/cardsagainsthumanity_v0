const Question = require('./question');
const Answer = require('./answer');
const sequelize = require('../sequelize');

const log = require('../../utils/log');

const LOG_TAG = 'DB';

sequelize.sync({force: false})
  .then(() => log.verbose(LOG_TAG, 'sync success'))
  .catch(e => {
    log.error(LOG_TAG, 'sync failed', e);
    process.exit(1);
  });

module.exports = {
  Question,
  Answer,
};
