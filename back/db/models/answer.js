let Sequelize = require('sequelize');
let sequelize = require('../sequelize');

module.exports = sequelize.define('answer', {

  text: {
    type: Sequelize.STRING,
    allowNull: false,
    required: true,
  },

}, {
  timestamps: false,
});