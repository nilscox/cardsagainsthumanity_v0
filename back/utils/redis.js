const redis = require('redis');
const log = require('./log');
const config = require('./../config');

const client = redis.createClient(config.redis.port, config.redis.host);

const LOG_TAG = 'REDIS';

client.on('connect', () => {
  log.verbose(LOG_TAG, 'connected');

  if (config.app.dev) {
    client.flushdb((err) => {
      if (err) {
        log.error(LOG_TAG, 'flush failed', err);
        process.exit(1);
      }
      log.verbose(LOG_TAG, 'flushed');
    });
  }
});

client.on('error', err => {
   log.error(LOG_TAG, err);
   process.exit(1);
});

client.setObj = ((k, obj) => {
  const value = JSON.stringify(obj);
  return new Promise((resolve, reject) => {
    client.set(k, value, (err) => {
      if (err) {
        log.error(LOG_TAG, err);
        reject(err);
      }
      else {
        resolve(obj);
      }
    });
  });
});

client.getObj = ((k) => {
  return new Promise((resolve, reject) => {
    client.get(k, (err, result) => {
      if (err) {
        log.error(LOG_TAG, err);
        reject(err);
      } else if (!result) {
        resolve(null);
      } else {
        const obj = JSON.parse(result.toString());
        resolve(obj);
      }
    });
  });
});

module.exports = client;
