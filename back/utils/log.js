const path = require('path');
const split = require('split');
const winston = require('winston');
const config = require('./../config');

function log(func, tag) {
  const args = [];

  for (let i = 2; i < arguments.length; ++i)
    args.push(arguments[i]);

  func('[' + new Date().toISOString() + '][' + tag + ']', ...args);
}

winston.level = config.log.level;

if (config.app.dev)
  winston.emitErrs = true;


const transports = [];

if (config.log.console) {
  transports.push(new winston.transports.Console({
    level: config.log.console,
    handleExceptions: true,
    json: false,
    colorize: true,
    humanReadableUnhandledException: true,
  }));
}

if (config.log.file) {
  transports.push(new winston.transports.File({
    level: 'info',
    filename: path.join(config.log.file.path, config.log.file.filename),
    handleExceptions: true,
    json: true,
    maxsize: 5242880, //5MB
    maxFiles: 5,
    colorize: false,
    timestamp: true,
  }));
}

const logger = new winston.Logger({
  transports: transports,
  exitOnError: false,
});

logger.stream = split().on('data', function (message) {
  module.exports.verbose('REQUEST', message);
});

module.exports = {
  logger,
  error:    log.bind(null, winston.error),
  warning:  log.bind(null, winston.warn),
  info:     log.bind(null, winston.info),
  verbose:  log.bind(null, winston.verbose),
  debug:    log.bind(null, winston.debug),
};
