const BAD_REQUEST           = 400;
const FORBIDDEN             = 401;
const UNAUTHORIZED          = 403;
const NOT_FOUND             = 404;
const INTERNAL_FATAL_ERROR  = 500;

const errors = {

  UKNOWN_ERROR: {
    status: INTERNAL_FATAL_ERROR,
    message: 'Uknown error'
  },

  GAME_ID_NOT_SET: {
    status: INTERNAL_FATAL_ERROR,
    message: 'The game id is not set',
  },

  GAME_ALREADY_JOINED: {
    status: BAD_REQUEST,
    message: 'You already joined this game'
  },

  GAME_ALREADY_STARTED: {
    status: BAD_REQUEST,
    message: 'The game has already started',
  },

  GAME_NOT_LEADER: {
    status: UNAUTHORIZED,
    message: 'You are not the game leader',
  },

  GAME_NOT_ENOUGH_PLAYERS_TO_START: {
    status: UNAUTHORIZED,
    message: 'There is not enough players to start the game (waiting for {0} more)',
  },

  GAME_NOT_FOUND: {
    status: NOT_FOUND,
    message: 'The game with id {0} does not exist',
  },

  GAME_NOT_JOINED: {
    status: FORBIDDEN,
    message: 'You did not join a game',
  },

  GAME_THIS_NOT_JOINED: {
    status: FORBIDDEN,
    message: 'You did not join this game',
  },

  GAME_CANNOT_ANSWER: {
    status: UNAUTHORIZED,
    message: 'You cannot answer',
  },

  GAME_ALREADY_SUBMITTED_ANSWER: {
    status: UNAUTHORIZED,
    message: 'You have already submitted an answer',
  },

  GAME_CANNOT_SUBMIT_ANSWER: {
    status: UNAUTHORIZED,
    message: 'You cannot submit an answer',
  },

  GAME_CANNOT_SUBMIT_THIS_ANSWER: {
    status: UNAUTHORIZED,
    message: 'You cannot submit this answer',
  },

  GAME_CANNOT_SELECT_ANSWER: {
    status: UNAUTHORIZED,
    message: 'You cannot select an answer',
  },

  GAME_CANNOT_SELECT_THIS_ANSWER: {
    status: UNAUTHORIZED,
    message: 'You cannot select an this answer',
  },

  GAME_CANNOT_SELECT_ANSWER_YET: {
    status: UNAUTHORIZED,
    message: 'You cannot select an answer yet',
  },

  GAME_NO_MORE_QUESTION: {
    status: INTERNAL_FATAL_ERROR,
    message: 'No more question available',
  },

  GAME_NO_MORE_ANSWER: {
    status: INTERNAL_FATAL_ERROR,
    message: 'No more answer available',
  },

  PLAYER_NICK_NOT_SET: {
    status: INTERNAL_FATAL_ERROR,
    message: 'Player nick is not set',
  },

  PLAYER_NOT_REGISTERED: {
    status: FORBIDDEN,
    message: 'You are not registered',
  },

  PLAYER_NOT_FOUND: {
    status: NOT_FOUND,
    message: 'Player with nick {0} not found',
  },

  PLAYER_NICK_NOT_AVAILABLE: {
    status: FORBIDDEN,
    message: 'this nick is not available',
  },

  INVALID_PARAMETER: {
    status: BAD_REQUEST,
    message: 'Invalid parameter {0}',
  },

};

class GameError extends Error {

  constructor(code, params = [], data = null) {
    if (!errors[code])
      throw new Error('Invalid error code: ' + code);

    const message = errors[code].message.replace(/{(\d+)}/g, (match, idx) => {
      return typeof params[idx] != 'undefined' ? params[idx] : match;
    });

    super(message);

    this.status = errors[code].status;
    this.data = data;
    this.code = code;
  }

}

module.exports = {
  GameError,
  errors: Object.keys(errors).reduce((obj, k) => {obj[k] = k; return obj;}, {}),
};
