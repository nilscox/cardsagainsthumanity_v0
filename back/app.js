const config = require('./config');

if (!config.app.dev)
  console.log = () => {};

const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const log = require('./utils/log');

const app = express();
const server = http.createServer(app);

app.set('x-powered-by', false);

app.use(morgan(':remote-addr -- :method :url -- :status (:response-time[3]ms)', { stream: log.logger.stream }));

app.use(express.static('www', { root: __dirname }));
app.use('/game/:id', express.static('www', { root: __dirname }));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(require('./session'));

app.use('/api', require('./api'));

if (config.dev) {
  const webpack = require('webpack');
  const webpackDevMiddleware = require('webpack-dev-middleware');
  const webpackHotMiddleware = require('webpack-hot-middleware');
  const webpackConfig = require('../webpack.config.js');

  const compiler = webpack(webpackConfig);

  app.use(webpackDevMiddleware(compiler, {
    noInfo: true,
    publicPath: webpackConfig.output.publicPath
  }));

  app.use(webpackHotMiddleware(compiler));
  app.set('json spaces', 2);
}

app.use((err, req, res, next) => {
  if (err) {
    console.error(err);
    res.status(500).end();
  }
});

module.exports = {
  app,
  server,
};
