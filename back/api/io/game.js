const { broadcast, join, leave } = require('.');

module.exports = {

  onJoin: (game, player) => {
    join(player.nick, game.id);
    broadcast(game.id, 'JOIN', {
      nick: player.nick,
    });
  },

  onLeave: (game, player) => {
    leave(player.nick, game.id);
    broadcast(game.id, 'LEAVE', {
      nick: player.nick,
    })
  },

  onStart: (game) => {
    broadcast(game.id, 'START', {
      id: game.id,
      players: game.players,
      currentPlayer: game.currentPlayer,
    });
  },

  onSubmit: (game, player) => {
    broadcast(game.id, 'SUBMIT', {
      nick: player.nick,
    });
  },

  onSelect: (game, answers, winner) => {
    broadcast(game.id, 'SELECT', {
      answers,
      winner,
    });
  },

};
