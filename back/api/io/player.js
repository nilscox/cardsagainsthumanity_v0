const { send, broadcast } = require('.');

module.exports = {

  onAnswers: (player, answers) => {
    send(player.nick, 'ANSWERS', answers);
  },

  onQuestion: (game, question) => {
    broadcast(game.id, 'QUESTION', question);
  },

};
