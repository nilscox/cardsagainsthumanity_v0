const sessionMiddleware = require('../../session');
const log = require('../../utils/log');

const LOG_TAG = 'SOCK';

let nsp = null;
const sockets = {};

const init = server => {
  log.verbose(LOG_TAG, 'initializing');

  const io = require('socket.io')(server);
  io.use((socket, next) => {
    sessionMiddleware(socket.request, socket.request.res, next)
  });

  nsp = io.of('/game');

  nsp.on('connection', (socket) => {

    if (!socket.request.session.player)
      return socket.emit('ERROR', { message: 'You are not registered' });

    const nick = socket.request.session.player;

    if (Object.keys(sockets).indexOf(nick) !== -1)
      return socket.emit('ERROR', { message: 'You are already connected' });

    log.verbose(LOG_TAG, 'socket for ' + nick + ' connected from ' + socket.handshake.address);
    sockets[nick] = socket;
  });
};


const socket = (nick) => {
  if (!sockets[nick])
    throw new Error('Missing socket for player ' + nick);
  return sockets[nick];
};

const broadcast = (gameid, event, data) => {
  log.verbose(LOG_TAG, 'broadcast to ' + gameid + ': ' + event, data);
  nsp.to(gameid).emit(event, data);
};

const send = (nick, event, data) => {
  log.verbose(LOG_TAG, 'send to ' + nick + ': ' + event, data);
  socket(nick).emit(event, data);
};

const join = (nick, gameid) => {
  log.verbose(LOG_TAG, 'socket for ' + nick + ' join ' + gameid);
  socket(nick).join(gameid);
};

const leave = (nick, gameid) => {
  log.verbose(LOG_TAG, 'socket for ' + nick + ' leave ' + gameid);
  socket(nick).leave(gameid);
};

module.exports = {
  init,
  send,
  broadcast,
  join,
  leave,
};