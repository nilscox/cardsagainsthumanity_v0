const router = require('express').Router();
const expressValidator = require('express-validator');
const log = require('../utils/log');

const config = require('../config');
const GameError = require('../utils/err').GameError;
const err = require('../utils/err').errors;
const redis = require('../utils/redis');

const LOG_TAG = 'APP';

router.use(expressValidator());

router.use('/game', require('./routes/game'));
router.use('/player', require('./routes/player'));

router.use((err, req, res, next) => {
  if (!(err instanceof GameError)) {
    if (config.app.dev)
      log.error(err);
    err = new GameError(err.UKNOWN_ERROR, [], { error: err });
  }

  if (!err.status)
    err.status = 500;

  if (!err.message)
    err.message = 'Unknown error';

  log.info(LOG_TAG, err.status, err.message);

  res.status(err.status).json(Object.assign(err.data || {}, {
    status: err.status,
    message: err.message,
  }));
});

module.exports = router;
