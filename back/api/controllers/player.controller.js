const log = require('../../utils/log');
const redis = require('../../utils/redis');
const GameError = require('../../utils/err').GameError;
const err = require('../../utils/err').errors;
const PlayerIO = require('../io/player');

const LOG_TAG = 'PLAYER';

module.exports = class Player {

  constructor(nick) {
    if (!nick)
      throw new GameError(err.PLAYER_NICK_NOT_SET);
    this.nick = nick;
  }

  static load(nick) {
    return redis.getObj(nick).then(obj => {
      if (!obj)
        return null;
      log.verbose(LOG_TAG, 'loaded', obj);
      return Object.assign(new Player(obj.nick), obj);
    });
  };

  save() {
    if (!this.nick)
      throw new GameError(err.PLAYER_NICK_NOT_SET);
    return redis.setObj(this.nick, this)
      .then(obj => {
        log.verbose(LOG_TAG, 'saved', obj);
        return obj;
      });
  };

  pickAnswers(answers) {
    answers.forEach(a => this.answers.push(a));
    PlayerIO.onAnswers(this, answers);
    return this.save();
  };

  pickQuestion(game, question) {
    PlayerIO.onQuestion(game, question);
  };

};