const uuid = require('node-uuid');
const log = require('../../utils/log');
const GameError = require('../../utils/err').GameError;
const err = require('../../utils/err').errors;
const redis = require('../../utils/redis');
const Question = require('../../db/models/index').Question;
const Answer = require('../../db/models/index').Answer;
const Player = require('./player.controller.js');
const GameIO = require('../io/game');

const LOG_TAG = 'GAME';

const TOTAL_CARDS = 3;
const MINIMUM_PLAYERS_REQUIRED = 2;

module.exports = class Game {

  constructor() {
    this.id = uuid.v4();
    this.questions = [];
    this.answers = [];
    this.players = [];
    this.currentPlayer = null;
    this.submittedAnswers = {};
  }

  static load(id) {
    return redis.getObj(id).then(obj => {
      if (!obj)
        return null;

      log.verbose(LOG_TAG, 'loaded', obj);
      return Object.assign(new Game(), obj);
    });
  }

  save() {
    if (!this.id)
      throw new GameError(err.GAME_ID_NOT_SET);

    return redis.setObj(this.id, this)
      .then(obj => {
        log.verbose(LOG_TAG, 'saved', obj);
        return obj;
      });
  }

  sanitize() {
    const obj = {
      id: this.id,
      players: this.players,
    };

    if (this.currentPlayer) {
      obj.currentPlayer = this.currentPlayer;
      obj.submittedAnswers = Object.keys(this.submittedAnswers);
    }

    return obj;
  }

  fetchCards() {
    return Question.findAll().then(questions => {
      this.questions = questions.map(q => q.get());
      return Answer.findAll();
    }).then(answers => this.answers = answers.map(a => a.get()));
  }

  fetchPlayers() {
    return Promise.all(this.players.map(nick => Player.load(nick)));
  }

  nextQuestion() {
    if (!this.questions.length)
      throw new GameError(err.GAME_NO_MORE_QUESTION);
    return this.questions.splice(0, 1)[0];
  }

  nextAnswers(n) {
    if (!this.answers.length)
      throw new GameError(err.GAME_NO_MORE_ANSWER);
    return this.answers.splice(0, n);
  }

  allAnswersSubmitted() {
    return Object.keys(this.submittedAnswers).length === this.players.length - 1;
  }

  dealCards(players) {
    if (Array.isArray(players))
      return Promise.all(players.map(this.dealCards.bind(this)));
    if (players.answers.length < TOTAL_CARDS)
      return players.pickAnswers(this.nextAnswers(TOTAL_CARDS - players.answers.length));
  }

  start(player) {
    if (this.currentPlayer)
      throw new GameError(err.GAME_ALREADY_STARTED);

    if (player.nick !== this.players[0])
      throw new GameError(err.GAME_NOT_LEADER, [], { leader: this.players[0] });

    if (this.players.length < MINIMUM_PLAYERS_REQUIRED)
      throw new GameError(err.GAME_NOT_ENOUGH_PLAYERS_TO_START, [MINIMUM_PLAYERS_REQUIRED - this.players.length]);

    this.currentPlayer = this.players[parseInt(Math.random() * this.players.length)];

    return this.fetchCards()
      .then(() => this.fetchPlayers())
      .then(players => this.dealCards(players))
      .then(() => Player.load(this.currentPlayer))
      .then(player => player.pickQuestion(this, this.nextQuestion()))
      .then(() => GameIO.onStart(this))
      .then(() => log.info(LOG_TAG, 'started', this))
      .then(() => this.save());
  }

  join(player) {
    if (this.players.indexOf(player.nick) !== -1)
      throw new GameError(err.GAME_ALREADY_JOINED, { game: this.sanitize() });

    player.answers = [];
    this.players.push(player.nick);

    GameIO.onJoin(this, player);

    return player.save()
      .then(player => log.info(LOG_TAG, 'player joined', player))
      .then(() => this.save());
  }

  leave(player) {
    const idx = this.players.indexOf(player.nick);

    if (idx === -1)
      throw new GameError(err.GAME_THIS_NOT_JOINED);

    delete player.answers;
    this.players.splice(idx, 1);

    GameIO.onLeave(this, player);

    return player.save()
      .then(player => log.info(LOG_TAG, 'player left', player))
      .then(() => this.save());
  }

  submit(player, answer_id) {
    if (this.currentPlayer === player.nick)
      throw new GameError(err.GAME_CANNOT_SUBMIT_ANSWER);

    if (this.submittedAnswers[player.nick])
      throw new GameError(err.GAME_ALREADY_SUBMITTED_ANSWER);

    const idx = player.answers.findIndex(a => a.id == answer_id);

    if (idx === -1)
      throw new GameError(err.GAME_CANNOT_SUBMIT_THIS_ANSWER);

    const answer = player.answers.splice(idx, 1)[0];
    this.submittedAnswers[player.nick] = answer;

    GameIO.onSubmit(this, player);

    return player.save()
      .then(() => log.info(LOG_TAG, 'answer submitted', answer))
      .then(() => this.save());
  }

  select(player, answer_id) {
    if (this.currentPlayer !== player.nick)
      throw new GameError(err.GAME_CANNOT_SELECT_ANSWER);

    if (!this.allAnswersSubmitted())
      throw new GameError(err.GAME_CANNOT_SELECT_ANSWER_YET);

    const idx = Object.values(this.submittedAnswers).findIndex(a => a.id === parseInt(answer_id));

    if (idx === -1)
      throw new GameError(err.GAME_CANNOT_SELECT_THIS_ANSWER);

    const winner = Object.keys(this.submittedAnswers)[idx];
    const answers = this.submittedAnswers;

    this.currentPlayer = winner;
    this.submittedAnswers = {};

    GameIO.onSelect(this, answers, winner);

    return this.fetchPlayers()
      .then(players => this.dealCards(players))
      .then(() => Player.load(this.currentPlayer))
      .then(player => player.pickQuestion(this, this.nextQuestion()))
      .then(() => log.info(LOG_TAG, 'answer selected for ' + winner, answers))
      .then(() => this.save());
  }

};