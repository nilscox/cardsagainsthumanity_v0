const router = require('express').Router();
const GameError = require('../../utils/err').GameError;
const err = require('../../utils/err').errors;
const Game = require('../controllers/game.controller.js');
const Player = require('../controllers/player.controller.js');

router.use((req, res, next) => {
  const nick = req.session.player;

  if (!nick)
    throw new GameError(err.PLAYER_NOT_REGISTERED);

  Player.load(nick).then(player => {
    if (!player)
      throw new GameError(err.PLAYER_NOT_FOUND, [nick]);
    req.player = player;
    next();
  }).catch(next);
});

router.get('/:id', (req, res, next) => {
  Game.load(req.params.id)
    .then(game => res.json(game.sanitize()))
    .catch(next);
});

router.post('/', (req, res, next) => {
  new Game().save()
    .then(game => res.json(game.sanitize()))
    .catch(next);
});

router.post('/join', (req, res, next) => {
  req.checkBody('id').notEmpty().isUUID(4);
  req.sanitizeBody('id').toString();

  req.getValidationResult()
    .then(result => {
      if (!result.isEmpty())
        throw new GameError(err.INVALID_PARAMETER, [result.array()[0].param]);
      return Game.load(req.body.id);
    })
    .then(game => {
      if (!game)
        throw new GameError(err.GAME_NOT_FOUND, [req.body.id]);
      req.game = game;
      return game.join(req.player);
    })
    .then(() => {
      req.session.game = req.game.id;
      res.json(req.game.sanitize());
    })
    .catch(next);
});

router.use((req, res, next) => {
  const id = req.session.game;

  if (!id)
    throw new GameError(err.GAME_NOT_JOINED);

  Game.load(id)
    .then(game => {
      if (!game)
        throw new GameError(err.GAME_NOT_FOUND, [id]);
      req.game = game;
      next();
    })
    .catch(next);
});

router.post('/start', (req, res, next) => {
  req.game.start(req.player)
    .then(() => res.json(req.game.sanitize()))
    .catch(next);
});

router.post('/submit', (req, res, next) => {
  req.checkBody('answer').notEmpty().isInt();
  req.sanitizeBody('answer').toInt();

  req.getValidationResult()
    .then(result => {
      if (!result.isEmpty())
        throw new GameError(err.INVALID_PARAMETER, [result.array()[0].param]);
      return req.game.submit(req.player, req.body.answer)
    })
    .then(() => res.json(req.game.sanitize()))
    .catch(next);
});

router.post('/select', (req, res, next) => {
  req.checkBody('answer', 'invalid parameter').notEmpty().isInt();
  req.sanitizeBody('answer').toInt();

  req.getValidationResult()
    .then(result => {
      if (!result.isEmpty())
        throw new GameError(err.INVALID_PARAMETER, [result.array()[0].param]);
      return req.game.select(req.player, req.body.answer);
    })
    .then(() => res.json(req.game.sanitize()))
    .catch(next);
});

module.exports = router;
