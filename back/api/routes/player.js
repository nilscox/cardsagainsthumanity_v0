const router = require('express').Router();
const GameError = require('../../utils/err').GameError;
const err = require('../../utils/err').errors;
const Player = require('../controllers/player.controller.js');

router.post('/', (req, res, next) => {
  req.checkBody('nick', 'invalid parameter').notEmpty().isAlphanumeric();
  req.sanitizeBody('nick').toString();

  req.getValidationResult()
    .then(result => {
      if (!result.isEmpty())
        throw new GameError(err.INVALID_PARAMETER, [result.array()[0].param]);
      return Player.load(req.body.nick);
    })
    .then(player => {
      if (player)
        throw new GameError(err.PLAYER_NICK_NOT_AVAILABLE, [], { nick: req.body.nick });
      return new Player(req.body.nick).save();
    }).then(player => {
      req.session.player = player.nick;
      res.json(player);
    }).catch(next);
});

router.get('/:nick', (req, res, next) => {
  req.checkBody('nick', 'invalid parameter').notEmpty().isAlphanumeric();
  req.sanitizeBody('nick').toString();

  req.getValidationResult()
    .then(result => {
      if (!result.isEmpty())
        throw new GameError(err.INVALID_PARAMETER, [result.array()[0].param]);
      return Player.load(req.body.nick)
    })
    .then(player => {
      if (!player)
        throw new GameError(err.PLAYER_NOT_FOUND, [req.body.nick]);
      res.json(player);
    }).catch(next);
});

module.exports = router;
