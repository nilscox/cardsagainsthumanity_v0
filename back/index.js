const app = require('./app').app;
const server = require('./app').server;
const log = require('./utils/log');
const config = require('./config');

const port = process.env.PORT || config.app.port || 4242;

log.info('APP', 'starting app on port ' + port);

require('./api/io').init(server);

server.listen(port);
