module.exports = {

  app: {
    dev: process.env.NODE_ENV === 'development',
    port: 4242,
  },

  log: {
    level: 'debug',
    console: {
      level: 'debug',
    },
    file: {
      level: 'info',
      path: __dirname,
      filename: 'app.log',
    }
  },

  db: {
    username: 'cah',
    password: 'cah',
    database: 'cah',
    host: 'mypostgres',
    port: 5432,
    dialect: 'postgresql'
  },

  redis: {
    host: 'myredis',
    port: 6379,
  },

};
