const session = require('express-session');

module.exports = session({
  secret: 'cah',
  resave: true,
  saveUninitialized: true
});
